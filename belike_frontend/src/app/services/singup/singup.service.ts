import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../models/user.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SingupService {

  private user$ = new BehaviorSubject<User>({
    username: '',
    password: ''
  });

  constructor(private http: HttpClient) {
    this.http = http;
  }

  getUser(): Observable<User> {
    return this.user$.asObservable();
  }

  setUser(user: User): void {
    this.user$.next(user);
  }

  singup(user: User): Observable<any> {
    let headers = { 'Content-Type': 'application/json'};
        
    return this.http.post('http://localhost:3000/api/singup', user, { headers });
  }
}
