import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) {
    this.http = http
  }

  getNews(): Observable<any> {
    let headers = { 'Content-Type': 'application/json'};
    
    return this.http.get('http://localhost:3000/api/news', { headers });
  }
}
