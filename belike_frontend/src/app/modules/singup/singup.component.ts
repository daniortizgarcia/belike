import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { SingupService } from 'src/app/services/singup/singup.service';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.scss']
})
export class SingupComponent implements OnInit {

  singupForm = this.fb.group({
    username: ['', [Validators.required, Validators.pattern(/[A-zçñCÑ0-9]{2,}/)]],
    password: ['', [Validators.required, Validators.minLength(7), Validators.pattern(/[A-Z]{1,}/), Validators.pattern(/[#]{1,}/)]],
    repeatPassword: ['', Validators.required]
  });

  userInfo: User;
  submitted: boolean;
  
  singupSubscription: Subscription;
  
  constructor(
    private fb: FormBuilder,
    private singupService: SingupService,
    private router: Router
  ) {
    this.fb = fb;
    this.singupService = singupService;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.singupSubscription && this.singupSubscription.unsubscribe();
  }

  register() {
    this.submitted = true;
    console.log(this.singupForm)
    if (this.singupForm.valid && this.singupForm.value.repeatPassword === this.singupForm.value.password) {
      this.singupSubscription = this.singupService.singup(this.singupForm.value).subscribe(
        (response) => {
          this.singupService.setUser({ id: response.id, username: response.username, password: response.password});
          this.router.navigate(['inicio'])
        },
        (err) => {
          console.log(err)
        }
      );
    }
  }

}
