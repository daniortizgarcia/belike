import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/news/news.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  news: any;

  constructor(
    private newsService: NewsService
  ) {
    this.newsService = newsService;
  }

  ngOnInit(): void {
    this.getNews();
  }

  getNews(): void {
    this.newsService.getNews().subscribe(
      (response) => {
        this.news = response;
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      }
    )
  }

}
