import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SingupService } from 'src/app/services/singup/singup.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  username: string;

  constructor(
    private singupService: SingupService,
    private router: Router
    ) {
    this.singupService = singupService;
    this.router = router;

    this.singupService.getUser().subscribe((user) => {
      this.username = user?.username;
    });
  }

  ngOnInit(): void {
    if (!this.username) {
      this.router.navigate(['singup'])
    }
  }

}
