import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { SingupComponent } from './modules/singup/singup.component';

const routes: Routes = [
  { path: 'inicio', component: HomeComponent},
  { path: 'singup', component: SingupComponent},
  { path: '**', redirectTo: 'singup'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
