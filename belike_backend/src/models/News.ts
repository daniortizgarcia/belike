import mongoose from 'mongoose';

interface INews {
  id?: string;
  title: string;
  body: string;
}

const newsSchema = new mongoose.Schema({
  id: {
    type: String,
    ref: 'News'
  },
  title: {
    type: String,
    required: true
  },
  body: {
    type: String,
    required: true
  }
});

const build = (attr: INews) =>  {
  return new News(attr) 
}

interface newsModelInterface extends mongoose.Model<NewsDoc> {
  build(attr: INews): NewsDoc
}

interface NewsDoc extends mongoose.Document {
  id: string;
  title: string;
  body: string;
}

newsSchema.statics.build = (attr: INews) => {
  return new News(attr)
}

const News = mongoose.model<NewsDoc, newsModelInterface>('News', newsSchema)

News.build({
  title: 'Titulo',
  body: 'Esto es una descripción'
})

export { News }
