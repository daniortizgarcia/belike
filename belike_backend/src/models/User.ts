import mongoose from 'mongoose';

interface IUser {
  id?: string;
  username: string;
  password: string;
  repeatPassword?: string;
}

const userSchema = new mongoose.Schema({
  id: {
    type: String,
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  repeatPassword: {
    type: String,
    required: false
  }
});

const build = (attr: IUser) =>  {
  return new User(attr) 
}
interface userModelInterface extends mongoose.Model<UserDoc> {
  build(attr: IUser): UserDoc
}

interface UserDoc extends mongoose.Document {
  id: string,
  username: string;
  password: string;
  repeatPassword?: string;
}

userSchema.statics.build = (attr: IUser) => {
  return new User(attr)
}

const User = mongoose.model<UserDoc, userModelInterface>('User', userSchema)

User.build({
  username: 'daniortiz',
  password: '12345',
  repeatPassword: '12345'
})

export { User }