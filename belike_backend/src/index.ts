import express from 'express';
import { json } from 'body-parser';
import { newsRouter } from './routes/news';
import { singupRouter } from './routes/singup';
import mongoose from 'mongoose';
import cors from 'cors';

import { User } from './models/User';
import { News } from './models/News';

const allowedOrigins = ['http://localhost:4200', 'http://127.0.0.1:4200'];
const options: cors.CorsOptions = {
  origin: allowedOrigins
};

const app = express();
app.use(json());
app.use(cors(options));
app.use(singupRouter);
app.use(newsRouter);

mongoose.connect('mongodb://localhost:27017', {}, () => {
  console.log('Connected to database')
})

// REMOVE ALL DATA
User.deleteMany({}).exec();
News.deleteMany({}).exec();

// SAVE USER TO TEST
let user = User.build({ username: 'daniortiz', password: '123456' });
user.save();

const loremIpsum = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

// SAVE NEWS TO TEST
const news = News.build({ id: '1', title: 'Titulo 1', body: loremIpsum})
const news2 = News.build({ id: '2', title: 'Titulo 2', body: loremIpsum})
const news3 = News.build({ id: '3', title: 'Titulo 3', body: loremIpsum})
const news4 = News.build({ id: '4', title: 'Titulo 4', body: loremIpsum})
const news5 = News.build({ id: '5', title: 'Titulo 5', body: loremIpsum})
const news6 = News.build({ id: '6', title: 'Titulo 6', body: loremIpsum})
const news7 = News.build({ id: '7', title: 'Titulo 7', body: loremIpsum})
const news8 = News.build({ id: '8', title: 'Titulo 8', body: loremIpsum})
const news9 = News.build({ id: '9', title: 'Titulo 9', body: loremIpsum})
const news10 = News.build({ id: '10', title: 'Titulo 10', body: loremIpsum})
news.save();
news2.save();
news3.save();
news4.save();
news5.save();
news6.save();
news7.save();
news8.save();
news9.save();
news10.save();

app.listen(3000, () => {
  console.log('server is listening on port 3000')
});