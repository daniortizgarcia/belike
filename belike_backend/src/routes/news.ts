import express, { Request, Response } from 'express';
import { News } from '../models/News';

const router = express.Router();

router.get('/api/news', [], async (req: Request, res: Response) => {
  const news = await News.find({})
  return res.status(200).send(news);
});

export { router as newsRouter };