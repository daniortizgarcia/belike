import express, { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import { User } from '../models/User';

const router = express.Router();

router.get('/api/singup', async (req: Request, res: Response) => {
  const user = await User.find({});
  return res.status(200).send(user);
});

router.post('/api/singup', async (req: Request, res: Response) => {
  let { username, password, repeatPassword } = req.body;
  
  const salt = await bcrypt.genSalt(10);
  
  const id = await bcrypt.hash(username+password, salt);
  password = await bcrypt.hash(password, salt);

  const user = User.build({ id, username, password });
  user.save().then((user) => res.status(200).send(user));
});

export { router as singupRouter };