# Belike

## Backend

Lo primero que vamos a hacer es entrar a la carpeta `belike_backend` para levantar el backend.

Para que funcione el backend necesitaremos tener instalado la base de datos de mongodb `https://docs.mongodb.com/manual/installation/`.

Una vez instalada la base de datos vamos a ejecutar un `npm install` una vez dentro de la carpeta.

Cuando finalice el comando vamos a lanzar un `npm start` y el proyecto estará visible en `http://localhost:3000/`.

Ahora vamos a levantar el frontend.

## Frontend

Vamos a entrar a la carpeta `belike_frontend` para levantar el frontend.

Vamos a ejecutar un `npm install` una vez dentro de la carpeta.

Cuando finalice el comando vamos a lanzar un `npm start` y el proyecto estará visible en `http://localhost:4200/`. 